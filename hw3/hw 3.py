
import time
import random
from logging import root
from typing import Type


class Array_Search:
    def __init__(self, array):
        self.array = array

    def init_array_search(self, val_array):
        self.array = Array_Search(val_array)

    def squential_search(self, key):

        idx = 0
        for num in self.array:
            if num == key:
                return idx
            idx = idx+1
        return False

    def bsearch(self, val):

        return False


class BST_Node:
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None


class BST:
    def __init__(self):
        self.root = None

    def init_bst(self, val):
        self.root = BST_Node(val)

    def insert(self, val):
        if (self.root is None):
            self.init_bst(val)
        else:
            self.insertNode(self.root, val)

    def insertNode(self, current, val):
        if val <= current.val:
            if current.left:
                self.insertNode(current.left,val)
            else:
                current.left =BST_Node(val)
        elif val > current.val:
            if current.right:
                self.insertNode(current.right,val)
        else:
            current.right = BST_Node(val)

        return False

    def bsearch(self, val):
        lo, hi = 0, len(self) - 1
        while lo <= hi:
            mid = (lo + hi) / 2  #
            if l[mid] < val:
                lo = mid + 1
            elif val < self[mid]:
                hi = mid - 1
            else:
                return mid


        return False

    def searchNode(self, current, val):
        if current is None:
            return False
        elif val== current.val:
            return True
        elif val< current.val:
            return self.searchNode(current.left, val)
        else:
            return self.searchNode (current.right, val)


    def delete(self, val, ):
        self.size= 0
        if self.size >1:
            Node= self.searchNode(self.root, val)
            if Node:
                self.delete(Node)
                self.size= self.size -1
            elif self.size == 1 and self.root.val== val:
                self.root= None
                self.size =self.size-1


        return False



class RBBST_Node:
    def __init__(self,key, val, color):
        self.val = val
        self.left = None
        self.right = None
        self.color = color
        self.key = key
        self.size = 1
        if color.upper() not in ("R", "B"):
            raise ValueError
        self.color = color.upper()



RED = True
BLACK = False


class RBBST:

    def get_min(self,val,color):
        self.val = val
        self.left = None
        self.right = None
        self.color = color
        if not self.left:
            return self
        node = self
        while node.left:
            node = node.left
        return node

    def get_max(self):

        if not self.right:
            return self
        node = self
        while node.right:
            node = node.right
        return node

    def __init__(self):
        self.RBBST = None
        self.root = None

    def init_rbbst(self, val, color):
        self.root = RBBST_Node(val, color)

    def is_red(self, current,):
        if not current:
            return False
        return (current.color == "R")


    def rotate_left(self, current):

        assert self.is_red(current.right)
        new_left_size = current.size - current.right.size + current.size(current.right.left)
        new_right_size = current.size
        right_node = current.right
        current.right = right_node.left
        right_node.left = current
        right_node.color = current.color
        current.color = "R"
        current.size = new_left_size
        right_node.size = new_right_size
        return right_node


    def rotate_right(self, current):
        assert self.is_red(current.left)
        new_right_size = current.size - current.left.size + current.size(current.left.right)
        new_left_size = current.size
        left_node = current.left
        current.left = left_node.right
        left_node.right = current
        left_node.color = current.color
        current.color = "R"
        current.size = new_right_size
        left_node.size = new_left_size
        return left_node



    def flip_colors(self, current):
        assert not self. is_red(current)
        assert self.is_red(current.left)
        assert self. is_red(current.right)
        self.current.color = "R"
        self.current.left.color = "B"
        self.current.right.color = "B"

        return False

    def insert(self, val):
        if (self.root is None):
            self.init_rbbst(val, RED)
        else:
            self.insertNode(self.root, val)

    def insertNode(self, current, val):
        if val < current.val:
            current.p.left =self.insertNode(current.left, val)
            current.size+=1
        elif val> current.val:
            current.right =self.insertNode(current.right, val)

        return False

    def bsearch(self, val):
        return self.searchNode(self.root,val)


    def searchNode(self, current, val):
        while current:
            if val == current.val:
                return current
            if val < current.val:
                node = current.left
            else:
                node = current.right
        return current


if __name__ == "__main__":


    set_sz = 10
    tut = BST()

    vals = random.sample(range(1, 100), set_sz)

    for idx in range(set_sz - 1):

        tut.insert(vals[idx])

    print (tut.bsearch(vals[1]))
    print(tut.bsearch(11))

    tut_rb = RBBST()

    for idx in range(set_sz - 1):

        tut_rb.insert(vals[idx])

    print (tut.bsearch(vals[1]))
    print(tut.bsearch(11))