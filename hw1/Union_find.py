

# 1. quick-find
# 2. quick-union
# 3. weighted QU
# 4. QU + path compression
# 5. weighted QU + path compression

import time
import random
#import matplotlib.pyplot as plt

class UF(object):
    """Union Find class

    """
    def find(self, p):
        return self.id[p]

    def qf_init(self, a,):
        while a!= self.id[a]:
              a=self.id[a]
              return a

    def pfind(self, a):
            root = a
            while root != self.id[root]:
                root = self.id[root]
                while a != root:
                    self.new = self.id[a]
                    self.id[a] = root
                    a = self.new
                    return root
    def __init__(self):
        self.id= []
        self.sz = []
        self.new =[]

    def qf_init(self,N):
        """initialize the data structure

        """
        self.count = N
        for x in range(N):
            self.id.append(x)

    def qu_init(self,i):
        """initialize the data structure

         """
        self.count = i
        for x in range(x):
            self.id.append(x)
            self.sz.append(1)

    def qf_union(self, p, q):
        """Union operation for Quick-Find Algorithm.

        connect p and q. You need to
        change all entries with id[p] to id[q]
        (linear number of array accesses)

        """
        id_p = self.find(p)
        if not self.qf_connected(p,q):
           for i in range(0, len(self.id)):
              if self.id[i] == id_p:
                 self.id[i]= self.id[q]


        return 1


    def qf_connected(self, p, q):
        """Find operation for Quick-Find Algorithm.
                simply test whether p and q are connected

                """
        return self.find(p) == self.find(q)

        #return self.id[p] == self.id[q]


    def qu_union(self, p, q):
        """Union operation for Quick-Union Algorithm.
         connect p and q.
         """
        root_p = self.qu_find(p)
        root_q= self.qu_find(q)
        if not self.qu_connected (p, q):
            self.id[root_p] =root_q
            return 1


    def qu_connected(self, p, q):
        """Find operation for Quick-Union Algorithm.
         test whether p and q are connected

         """
        if self.qu_find(p)== self.qu_find(q):
             return True


    def wqu_union(self, p, q):
        """Union operation for Weighted Quick-Union Algorithm.
         connect p and q.

          """
        root_p = self.qu_find(p)
        root_q = self.qu_find(q)
        if not   self.qu_connected (p, q):

               if self.sz[q] > self.sz[p]:
                   self.id[root_p] = root_q
                   self.sz[q] > self.sz[p]
               else:
                   self.id[root_q] = root_q
                   self.sz[q] += self.sz[q]
                   self.count -= 1


    def wqu_connected(self, p, q):
        """Find operation for Weighted Quick-Union Algorithm.
         test whether p and q are connected

         """
        return self.qu_find(q)== self.qu_find(q)



    def pqu_union(self, p, q):
        """Union operation for path compressed Quick-Union Algorithm.
         connect p and q.

         """
        root_p = self.pfind(p)
        root_q = self.pfind(q)
        if self.pqu_connected (p,q):
         return 1
        else:
            self.id[root_p] = root_q
            self.count -= 1

    def pqu_connected(self, p, q):
        """Find operation for path compressed Quick-Union Algorithm.
         test whether p and q are connected

         """
        return self.pfind(p) == self.pfind(q)


    def wpqu_union(self, p, q):
        """Union operation for Weighted path compressed Quick-Union Algorithm.
         connect p and q.

         """
        root_p = self.pfind(p)
        root_q = self.pfind(q)
        if  self.sz[root_p] < self.sz[root_q]:
            self.id[root_p] = root_q
            self.sz[root_q] += self.sz[root_p]
        else:
            self.id[root_q] = root_p
            self.sz[root_p] += self.sz[root_q]


            return 1


    def wpqu_connected(self, p, q):
        """Find operation for Weighted path compressed Quick-Union Algorithm.
         test whether p and q are connected

         """
        return self.pfind(p) == self.pfind(q)
if __name__ == "__main__":

    # iteration
    set_sz = [10]
    timing = []

    # gives the timing for union operation only, you might want to do this for all functions you wrote.
    for set_sz in set_sz:
        # initialize network nodes
        inodes = UF()
        inodes.qf_init(set_sz)

        t0 = time.time()

        for idx in range(set_sz - 1):
            rp = random.randint(0, set_sz - 1)
            rq = random.randint(0, set_sz - 1)

            inodes.qf_union(rp, rq)

        t1 = time.time()

        total_time = t1 - t0

        timing.append(total_time)

        print(total_time)

    # this plots things in log scale (pls google it), you need to add matplotlib to your virtualenv first!


    # plt.plot(set_szs, timing)
    # plt.xscale('log')
    # plt.yscale('log')
    # plt.title('log')
    # plt.ylabel('some numbers')
    # plt.show()