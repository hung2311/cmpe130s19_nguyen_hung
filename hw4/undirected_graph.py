class Vertex:
    def __init__(self, vertex):
        self.name = vertex
        self.neighbors = []

    def add_neighbor(self, neighbor):
        if isinstance (neighbor,Vertex):
            self.neighbors.append(neighbor.name)
            neighbor.neighbors.append(self.name)
            self.neighbors =sorted(self.neighbors)
            neighbor.neighbors =sorted(neighbor.neighbors)
        else:
            return False

    def add_neighbors(self, neighbors):
        for neighbors in neighbors:
            if isinstance(neighbors,Vertex):
                if neighbors.name not in self.neighbors:
                    self.neighbors.append(neighbors.name)
                    neighbors.neighbors.append(self.name)
                    self.neighbors = sorted(self.neighbors)
                    neighbors.neighbors =sorted(neighbors.neighbors)

        return 1

    def __repr__(self):
        return str(self.neighbors)


class Graph:
    def __init__(self):
        self.vertices = {}

    def add_vertex(self, vertex):
        if isinstance(vertex,Vertex):
            self.vertices[vertex.name]= vertex.neighbors
        return 1

    def add_vertices(self, vertices):
        for vertex in vertices:
            if isinstance(vertex,Vertex):
                self.vertices[vertex.name] = vertex.neighbors
        return 1

    def add_edge(self, vertex_from, vertex_to):
        if isinstance(vertex_from,Vertex) and isinstance(vertex_to,Vertex):
            vertex_from.add_neighbor(vertex_to)
            if isinstance(vertex_from,Vertex) and isinstance(vertex_to,Vertex):
                self.vertices[vertex_from.name]= vertex_from.neighbors
                self.vertices[vertex_from.name]= vertex_to.neighbors
        return 1

    def add_edges(self, edges):
        for edges in edges:
            self.add_edge(edges[0],edges[1])
        return 1

    def adjacencyList(self):  # to represent the graph as adjacent list
        if len(self.vertices) >= 1:
            return [str(key) + ":" + str(self.vertices[key]) for key in self.vertices.keys()]
        else:
            return dict()





def graph(g):
    """ Function to print a graph as adjacency list and adjacency matrix. """
    return str(g.adjacencyList()) + '\n'