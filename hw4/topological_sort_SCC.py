from collections import defaultdict


# Class to represent a graph
class Graph:
    def __init__(self, vertices):
        self.graph = defaultdict(list)  # dictionary containing adjacency List
        self.V = vertices  # No. of vertices

    # function to add an edge to graph
    def addEdge(self, u, v):
        self.graph[u].append(v)

    def topological_SortUtil(self, v, visited, stack):
        visited[v] = True
        for i in self.graph[v]:
            if visited[i] == False:
                self.topological_SortUtil(i, visited, stack)

        # Push current vertex to stack which stores result
        stack.insert(0, v)

    def topological_Sort(self):
        visited = [False] * self.V
        stack = []
        for i in range(self.V):
            if visited[i] == False:
                self.topological_SortUtil(i, visited, stack)

        return 1


    def SCC(self):  # strongly connected components
        stack =[]
        visited =[False]*(self.v)
        for i in range(self.V):
            if visited[i]==False:
               self.topological_SortUtil(i,visited,stack)
        while stack:
            i=stack.pop()
            if visited[i] ==False:

        return 1