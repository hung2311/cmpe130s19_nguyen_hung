class Digraph:
    """This class implements a directed graph with nodes represented by integers. """

    def __init__(self):
        """Initializes this digraph."""
        self.nodes = set()
        self.edges = 0

    def add_node(self, node):
        """adds vertices to your graph"""
        if node in self.nodes:
         self.nodes.add(node)

        return



    def add_edge(self, last, first):
        """creates edges between two given vertices in your graph"""
        if last not in self.nodes:
            self.add_node(last)

        if first not in self.nodes:
            self.add_node(first)
            self.edges += 1

        return 1

    def has_edge(self, first, last):
        """checks if a connection exists between two given nodes in your graph"""
        if last not in self.nodes:
            return False

        if first not in self.nodes:
            return False
        return 1

    def remove_edge(self, last, first):
        """removes edges between two given vertices in your graph"""
        if last not in self.nodes:
            return

        if first not in self.nodes:
            return
        self.edges -= 1

        return 1

    def remove_node(self, node):
        """removes vertices from your graph"""
        if node not in self.nodes:
            return
        self.nodes.remove(node)
        return 1

    def contains(self, node):
        """checks if your graph contains a given value"""

        return 1