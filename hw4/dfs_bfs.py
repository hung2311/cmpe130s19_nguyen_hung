from collections import defaultdict

class Graph:
	def __init__(self):
		self.graph = defaultdict(list)

	def addEdge(self,u,v):
            self.graph[u].append(v)
    def Dfsutil ( self,v, visited):
        visited[v] =True
          print v,
        for i in self.graph[v]:
            if visited[i]== False:
                self.Dfsutil(i,visited)

    def dfs(self, v):
        visited = [False]* (len(self.graph))
        self.Dfsutil(v, visited)
        return 1

    def bfs(self, v):
        visited =[False* (len(self.graph))
        queue=[]
        queue.append (v)
        visited[v]= True
        while queue:
            v= queue.pop(0)
            print (v,end=" ")
            for i in self.graph[v]:
                if visited[i] ==False:
                    queue.append(i)
                    visited[i] =True

		return 1